import React, { Component } from 'react';
import SwapiServise from '../../services/swapi-service';
import './item-list.css';
import Loader from '../loader/loader'

export default class ItemList extends Component {
  state = {
    data: [],
    loading: true,
  }

  swapi = new SwapiServise()

 componentDidMount = () => {
    this.props.getData().then((data) => {
      this.setState({
        data: data,
         loading: false,
      })
    })
  }

 render() {
    const content = this.state.data.map((item) => {
      return (
        <li key={item.id} onClick={() => this.props.onSelectItem(item.id)} className="list-group-item">
          {this.props.children(item)}
        </li>
      )
    })

    if (this.state.loading) {
      return <Loader/>
    } else {
      return (
          <ul className="item-list list-group">
            {content}
          </ul>
      );
    }
  }
}