import React, { Component } from 'react';
import './item-details.css';

import Loader from '../loader/loader';

const Record = ({item, label, fieldName}) => {
    return (
        <li className="list-group-item">
            <span className="term">{label}</span>
            <span>{item[fieldName]}</span>
        </li>
    )
}

class ItemDetails extends Component {
  state = {
    data: {},
    loading: true,
  }

  componentDidMount = () => {
    const id = this.props.selectedItem
    this.props.getData(id).then(data => {
      this.setState({
        data: data,
        loading: false,
      })
    })
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.selectedItem != prevProps.selectedItem) {
      const id = this.props.selectedItem
      this.props.getData(id).then(data => {
        this.setState({
          data: data
        })
      })
    }
  }

 render() {
    const {id, name, gender, birthYear, eyeColor} = this.state.data;
    const imageUrl = this.props.getImage(id)

    if (this.state.loading) {
      return <Loader/>
    } else {
      return (
      <div className="person-details card">
        <img className="person-image" src={imageUrl} />

        <div className="card-body">
          <h4>{name}</h4>
          <ul className="list-group list-group-flush">

            {
                React.Children.map(this.props.children, (item) => {
                return React.cloneElement(
                  item,
                  {item: this.state.data},
                )
              })
            }

          </ul>
        </div>
      </div>
    )
    }
  }
}
export {Record, ItemDetails}