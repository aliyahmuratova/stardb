import React, { Component } from 'react';
import preloader from '../../assets/images/ripple.svg'

const Loader = () => {
  return <img src={preloader} alt=""/>
}
export default Loader;