import React from 'react';
import {PersonDetails, PeopleList} from '../sw-components';
import {withRouter} from "react-router-dom";


class PeoplePage extends React.Component {
  onSelectItem = (id) => {
    this.props.history.push(id)
  }

  render() {

    return (
      <div className="row mb2">
        <div className="col-md-6">
          <PeopleList onSelectItem={this.onSelectItem}/>
        </div>
        <div className="col-md-6">
            <PersonDetails selectedItem={this.props.selectedItemId} />

        </div>
      </div>
    )
  }
}

export default withRouter(PeoplePage)