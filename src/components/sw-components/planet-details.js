import React from 'react';
import {ItemDetails, Record} from '../item-details';
import withSwapiService from '../hoc';


const PlanetDetails = ({swapi, selectedItem}) => {
  const element = (
    <ItemDetails getData={swapi.getPlanet} getImage={swapi.getPlanetImage} selectedItem={selectedItem}>
      <Record label='Name' fieldName='name' />
      <Record label='Population' fieldName='population' />
      <Record label='Rotation period' fieldName='rotationPeriod' />
      <Record label='Diameter' fieldName='diameter' />
    </ItemDetails>
  )

  return element;
}

export default withSwapiService(PlanetDetails);
