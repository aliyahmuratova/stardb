import React from 'react';
import {Consumer} from "../swapi-context";


const withSwapiService = (Comp) => {
    return (props) => {
        const element = (
            <Consumer>
                {
                    (swapi) => {
                      return  <Comp {...props} swapi={swapi} />
                    }
                }
            </Consumer>
        )
        return element
    }
}

export default withSwapiService