import React from 'react';
import {ItemDetails, Record} from '../item-details';
import withSwapiService from '../hoc';

const StarshipDetails = ({swapi, selectedItem}) => {
  const element = (
    <ItemDetails getData={swapi.getStarship} getImage={swapi.getStarshipImage} selectedItem={selectedItem}>
      <Record label='Name' fieldName='name' />
      <Record label='Model' fieldName='model' />
      <Record label='Crew' fieldName='crew' />
      <Record label='Length' fieldName='length' />
      <Record label='Passengers' fieldName='passengers' />
      <Record label='Cargo capacity' fieldName='cargoCapacity' />
      <Record label='Manufacturer' fieldName='manufacturer' />
      <Record label='Cost in credits' fieldName='costInCredits' />
    </ItemDetails>
  )

  return element;
}

export default withSwapiService(StarshipDetails);
