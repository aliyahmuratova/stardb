import React from 'react';
import {ItemDetails, Record} from "../item-details";
import withSwapiService from "../hoc";

const PersonDetails = ({swapi, selectedItem})=>{
   const element = (
       <ItemDetails getData={swapi.getPerson} getImage={swapi.getPersonImage} selectedItem={selectedItem}>
           <Record label='Name' fieldName='name' />
           <Record label='Eye color' fieldName='eyeColor' />
           <Record label='Gender' fieldName='gender' />
           <Record label='Birth year' fieldName='birthYear' />
       </ItemDetails>
   )
return element;
}
export default withSwapiService(PersonDetails);
