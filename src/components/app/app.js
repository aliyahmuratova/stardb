import React from 'react';

import SwapiService from '../../services/swapi-service'
import {Provider} from "../swapi-context";
import Header from '../header';
import RandomPlanet from '../random-planet';
import {PeoplePage, PlanetsPage, StarshipsPage} from '../pages';
import {BrowserRouter as Router, Route} from "react-router-dom";

import './app.css';

const swapi = new SwapiService();

const App = () => {
  return (
      <Provider value={swapi}>
        <div>
            <Router>
            <Header />
            <RandomPlanet />
            <Route path='/' exact={true} render={() => <h1>This is the main page</h1>}  />
            <Route path='/people/:id?' render={({match}) => {
                const id = match.params.id || 1;
                return <PeoplePage selectedItemId={id}/>
            }}/>
            <Route path='/planets/' component={PlanetsPage}/>
            <Route path='/starships/:id?' render={({match}) => {
                const id = match.params.id || 9;
                return <StarshipsPage selectedItemId={id}/>
            }}/>
            </Router>
         </div>
      </Provider>
  );
};

export default App;
