import React, { Component } from 'react';
import SwappiService from '../../services/swapi-service';
import './random-planet.css';
import Loader from '../loader/loader';


export default class RandomPlanet extends Component {
  swapi = new SwappiService()

  state = {
    data: {},
    loading: true,
  }

  updatePlanet = () => {
    const randomId = Math.floor(Math.random() * (20 - 1) + 1);
    this.swapi.getPlanet(randomId).then((data) => {
      this.setState({
        data: data,
        loading: false
      })
    })
  }

  componentDidMount = () => {
    this.updatePlanet()
    this.myInterval = setInterval(
      () => {this.updatePlanet()},
      2000
    )
  }

  componentWillUnmount = () => {
    clearInterval(this.myIntervals)
  }

  render() {
    const {id, name, population, rotationPeriod, diameter} = this.state.data;
    const imgUrl = `https://starwars-visualguide.com/assets/img/planets/${id}.jpg`

    if (this.state.loading) {
      return <Loader />
    } else {
      return (
        <div className="random-planet jumbotron rounded">
          <img className="planet-image"
               src={imgUrl} />
          <div>
            <h4>{name}</h4>
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                <span className="term">Population</span>
                <span>{population}</span>
              </li>
              <li className="list-group-item">
                <span className="term">Rotation Period</span>
                <span>{rotationPeriod}</span>
              </li>
              <li className="list-group-item">
                <span className="term">Diameter</span>
                <span>{diameter}</span>
              </li>
            </ul>
          </div>
        </div>
      );
    }
  }
}
