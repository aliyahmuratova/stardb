import React from 'react';
import ItemList from "../item-list";
import withSwapiService from '../hoc';


const PeopleList = withSwapiService(({swapi, onSelectItem}) => {
  return (
    <ItemList getData={swapi.getAllPeople} onSelectItem={onSelectItem}>
      {(item) => `${item.name} - (${item.gender})`}
    </ItemList>
  )
})


const PlanetsList = withSwapiService(({swapi, onSelectItem}) => {
  return (
    <ItemList getData={swapi.getAllPlanets} onSelectItem={onSelectItem}>
      {(item) => `${item.name} - (${item.diameter})`}
    </ItemList>
  )
})

const StarshipsList = withSwapiService(({swapi, onSelectItem}) => {
  return (
    <ItemList getData={swapi.getAllStarships} onSelectItem={onSelectItem}>
      {(item) => `${item.name} - (${item.model})`}
    </ItemList>
  )
})

export {PeopleList, PlanetsList, StarshipsList}