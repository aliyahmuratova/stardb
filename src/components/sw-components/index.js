import PersonDetails from './person-details';
import PlanetDetails from './planet-details';
import StarshipDetails from './starship-details';
import {PeopleList, PlanetsList, StarshipsList} from './item-lists';

export {PersonDetails, PlanetDetails,StarshipDetails, PeopleList, PlanetsList, StarshipsList}