import React from 'react';
import {PlanetDetails, PlanetsList} from '../sw-components'


class PlanetsPage extends React.Component {
  state = {
    selectedItem: 2
  }

  onSelectItem = (id) => {
    this.setState(
      {
        selectedItem: id
      }
    )
  }

  render() {

    return (
      <div className="row mb2">
        <div className="col-md-6">
          <PlanetsList onSelectItem={this.onSelectItem}/>
        </div>
        <div className="col-md-6">
            <PlanetDetails selectedItem={this.state.selectedItem} />
        </div>
      </div>
    )
  }
}

export default PlanetsPage